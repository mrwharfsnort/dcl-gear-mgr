var fs = require("fs");

function merge(addMe1, addMe2){

  for (var cntr=0; cntr<4; cntr++){
    var pushMe = addMe2.cols[cntr];
    var target = addMe1.cols[cntr];
    pushMe.forEach(function(candidate){
      if (target.indexOf(candidate)==-1){
        target.push(candidate);
      }
      else{
        //console.log("hmm");
      }
    });
  }

}

function printCol(col){
  var returnMe = "";
  col.forEach(function(val){
    returnMe+="/"+val;
  });
  return returnMe.substr(1);
}

function rollup(sCsv) {
  var aLines = sCsv.split(/\r?\n/);
  var guns = {};
  var newLines = [];
  aLines.forEach(
    function (line) {
      if (line.trim().length==0) return;
      var aEntries = line.split(",");
      var name = aEntries.shift();
      var cols = [];
      //only grab 4 cols
      for (var cntr = 0; cntr < 4; cntr++) {
        var slashPerks = aEntries[cntr];
        var aPerk = slashPerks.split("/");
        cols.push(aPerk);
      }

      var addMe = {
        name: name,
        cols: cols
      };
      if (!guns[name]) {
        guns[name] = addMe;
      }
      else {
        //append addMe to existing entry
        merge(guns[name], addMe);
      }
    });
  for (var key in guns) {
    if (guns.hasOwnProperty(key)) {
      var obj = guns[key];
      var line = key+",";
      obj.cols.forEach(function(col){
        line+=printCol(col)+",";
      });
      line = line.substr(0, line.length-1);
      newLines.push(line);
    }
  }
  newLines.sort();
  var returnMe = "";
  newLines.forEach(function(line){
    returnMe+=line+"\n";
  });


  return returnMe;
}

var sCsv = fs.readFileSync("pveWeapons.csv", "utf-8");
var sData = rollup(sCsv);
fs.writeFileSync("pve.rollup.csv", sData, "utf-8");

sCsv = fs.readFileSync("pvpWeapons.csv", "utf-8");
sData = rollup(sCsv);
fs.writeFileSync("pvp.rollup.csv", sData, "utf-8");
console.log("Done");
