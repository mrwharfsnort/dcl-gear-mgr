/**
 * Created by davecaslin on 1/5/17.
 */


/**
 * Created by davecaslin on 1/2/17.
 */

(function(i, s, o, g, r, a, m) {
  i['GoogleAnalyticsObject'] = r;
  i[r] = i[r] || function() {
      (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
  a = s.createElement(o), m = s.getElementsByTagName(o)[0];
  a.async = 1;
  a.src = g;
  m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');


var GOOG_ANAL = 'UA-21710078-3';

function pushAd(note){
  try{

    (adsbygoogle = window.adsbygoogle || []).push({});
  }
  catch(e){
    console.log("Error pushing ad '"+note+"' "+e);
  }
}

function pushAds(){
  ga('create', GOOG_ANAL, 'auto');
  ga('send', 'pageview');
  pushAd("one");
}

function sendPageView(realUrl){
  try{
    ga('send', 'pageview', "/gearMgr/"+realUrl);
  }
  catch(e){
    console.log(e);
  }
}
