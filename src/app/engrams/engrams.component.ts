import {Component, ViewChild, AfterViewInit} from '@angular/core';

import {GearService} from '../services/gear.service';
import {Column} from '../grid/column';
import {GridComponent} from "../grid/grid.component";

@Component({
  selector: 'engrams',
  styleUrls: ['./engrams.component.css'],
  templateUrl: './engrams.component.html'
})

export class EngramsComponent implements AfterViewInit{

  @ViewChild('grid') grid: GridComponent;

  constructor(private gearService: GearService) {

    this.gearService.refreshedFeed.subscribe(
      x => {
        this.grid.processList(this.gearService.engrams, null);
      }
    );

  }
  readonly cols: Column[] = [
    new Column('', 'light', true, true),
    new Column('Name', 'name', true),
    new Column('Type', 'bucketName', false, false, true),
    new Column('Mark', 'mark', true, true)
  ];


  ngAfterViewInit() {
    this.grid.showCount = -1;
    this.grid.processList(this.gearService.engrams, null);
  }

}
