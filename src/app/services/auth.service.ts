/**
 * Created by Dave on 12/20/2016.
 */

import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, ResponseContentType, RequestMethod} from '@angular/http';
import {BungieResponse} from './model/bungie-response';
import {Auth} from './model/auth';

import { environment } from '../../environments/environment';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class AuthService {

  private auth: Auth = null;

  constructor(private http: Http) {
  }

  private refreshToken(): Promise<string> {
    let self: AuthService = this;
    let opts: RequestOptions = new RequestOptions(
      {
        method: RequestMethod.Post,
        withCredentials: true,
        responseType: ResponseContentType.Json,
        headers: new Headers({
          'X-API-Key': environment.apiKey
        })
      });

    return self.http.post('https://www.bungie.net/Platform/App/GetAccessTokensFromRefreshToken/',
      {refreshToken: self.auth.refreshToken.value},
      opts)
      .map(
        function (res) {
          let obj: Object = res.json();
          let bRes: BungieResponse = new BungieResponse(obj);
          let oAuth: Object = bRes.Response;
          let auth: Auth = new Auth(oAuth);
          let inception: Date = new Date();
          auth.accessToken.inception = inception;
          auth.refreshToken.inception = inception;
          self.auth = auth;
          localStorage.setItem("authorization", JSON.stringify(auth));
          return auth.accessToken.value;
        }
      ).toPromise();
  }

  signOut() {
    localStorage.removeItem("authorization");
    window.location.reload();
  }

  getKey(): Promise<string> {
    if (this.auth == null) {
      AuthService.reroute();
      return Promise.reject("Not authorized with Bungie.net");;
    }
    if (this.auth.accessToken.isValid()) {
      return Promise.resolve(this.auth.accessToken.value);
    }
    else if (this.auth.refreshToken.isValid()) {
      return this.refreshToken();
    }
    else {
      AuthService.reroute();
      return Promise.reject("Not authorized with Bungie.net");
    }
  }

  public static reroute() {
    //debugger;
    console.log("Rerouting to auth page.");
    window.location.href = environment.allowAuthUrl;
  }

  init(): Promise<string> {

    //let self: AuthService = this;
    let sAuth: string = localStorage.getItem("authorization");
    //need to auth
    if (sAuth == null || sAuth.length == 0) {
      //we're loading a new page at this point
      AuthService.reroute();
      return Promise.reject("Not authorized with Bungie.net");
    }
    let oAuth: Object = JSON.parse(sAuth);
    let auth: Auth = new Auth(oAuth);
    this.auth = auth;
    console.dir("Refresh token valid: " + auth.refreshToken.isValid());
    console.dir("Access token valid: " + auth.accessToken.isValid());
    return this.getKey();
  }
}
