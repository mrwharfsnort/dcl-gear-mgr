/**
 * Created by davecaslin on 12/21/16.
 */
import {Injectable} from "@angular/core";

import {Subclass, Bounty} from './item';


export interface Tier{
  value: number;
  tier: number;
}

export interface Tiers{
  int: Tier;
  str: Tier;
  dis: Tier;
}

export interface Stats{
  agility: number;
  armor: number;
  recovery: number;
}

export abstract class Target{
  public readonly label: string;
  public readonly id: string;

  constructor(label: string, id: string){
    this.label = label;
    this.id = id;
  }
}

export class Vault extends Target{
  constructor(){
    super("Vault", "vault");
  }
}

export class Vendor extends Target{
  constructor(){
    super("Vendor", "vendor");
  }
}


export class Character extends Target{
  public readonly backgroundPath: string;
  public readonly emblemPath: string;
  public readonly className: string;
  public readonly gender: string;
  public readonly level: number;

  public readonly minutesPlayed: number;
  public readonly light: number;
  public readonly race: string;

  public readonly stats: Stats;
  public readonly tier: Tiers;
  public subclass: Subclass[];
  public equippedSubclass: Subclass;

  public bounties: Bounty[];

  public totalTier(){
    return this.tier.int.tier+this.tier.dis.tier+this.tier.str.tier;
  }

  static getTier(value: number): number{
    if (value<60) return 0;
    if (value<120) return 1;
    if (value<180) return 2;
    if (value<240) return 3;
    if (value<300) return 4;
    return 5;
  }

  //stats
  //int/dis/str
  //agil/armor/recovery

  constructor(char: any, cache: any) {
    let base: any = char.characterBase;
    let className: string = cache.Class[base.classHash].className;
    let light: number = base.powerLevel as number;
    super(className+"("+light+")", base.characterId);
    this.bounties = [];
    this.level = char.baseCharacterLevel;
    this.subclass = [];
    this.backgroundPath = char.backgroundPath;
    this.emblemPath = char.emblemPath;
    this.className = className;
    this.gender = cache.Gender[base.genderHash].genderName.substring(0, 1);
    this.minutesPlayed = Number(base.minutesPlayedTotal);
    this.light = light;
    this.race = cache.Race[base.raceHash].raceName.substring(0, 3);

    this.stats = {
      agility: base.stats.STAT_AGILITY?base.stats.STAT_AGILITY.value:0,
      armor: base.stats.STAT_ARMOR?base.stats.STAT_ARMOR.value:0,
      recovery: base.stats.STAT_RECOVERY?base.stats.STAT_RECOVERY.value:0
    };
    this.tier = {
      int: {
        value: base.stats.STAT_INTELLECT.value,
        tier: Character.getTier(base.stats.STAT_INTELLECT.value)
      },
      dis: {
        value: base.stats.STAT_DISCIPLINE.value,
        tier: Character.getTier(base.stats.STAT_DISCIPLINE.value)
      },
      str: {
        value: base.stats.STAT_STRENGTH.value,
          tier: Character.getTier(base.stats.STAT_STRENGTH.value)
      }
    };
  }

}
