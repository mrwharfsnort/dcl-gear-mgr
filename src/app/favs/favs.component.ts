import {Component, ViewChild, AfterViewInit} from '@angular/core';

import {GearService} from '../services/gear.service';
import {Column} from '../grid/column';
import {GridComponent} from "../grid/grid.component";
import {MarkService} from "../services/mark.service";
import {Item} from "../services/model/item";

@Component({
  selector: 'favs',
  styleUrls: ['./favs.component.css'],
  templateUrl: './favs.component.html'
})

export class FavsComponent implements AfterViewInit{

  @ViewChild('grid') grid: GridComponent;

  constructor(private gearService: GearService, private markService: MarkService) {
  }

  private processList(){
    let items: Item[] = [];
    this.gearService.weapons.forEach(
      x=> {
        if (x.fav){
          items.push(x);
        }
      }
    );
    this.gearService.armor.forEach(
      x=> {
        if (x.fav){
          items.push(x);
        }
      }
    );
    this.grid.processList(items, null);
  }

  readonly cols: Column[] = [
    new Column('Light', 'light', true),
    new Column('Name', 'name', true),
    new Column('Type', 'type', true, false, true, true),
    new Column('Perks', 'steps', true, true, true, true),
    new Column('Mark', 'mark', true, true)
  ];

  ngAfterViewInit() {
    this.gearService.refreshedFeed.subscribe(
      x => {
        this.processList();
      }
    );
    this.markService.favsUpdatedFeed.subscribe(
      x => {
        this.processList();
      }
    );

    this.processList();
  }

}
