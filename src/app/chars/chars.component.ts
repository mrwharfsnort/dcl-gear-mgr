import {Component, ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';
import {GearService} from '../services/gear.service';
import {Character} from '../services/model/character';

@Component({
    selector: 'chars',
    styleUrls: ['./chars.component.css'],
    templateUrl: './chars.component.html'
})

export class CharsComponent {
    
    chars: Character[] = []; 
    
    constructor(private gearService: GearService) {
        this.chars = this.gearService.chars;
        this.gearService.refreshedFeed.subscribe(
            x => {
                this.chars = this.gearService.chars;                
            }
        );
    }
}
