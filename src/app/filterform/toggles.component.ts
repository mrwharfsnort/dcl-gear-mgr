import {Component, EventEmitter, Input, Output} from "@angular/core";
import {Choice} from "./filter";


@Component({
  selector: 'toggles',
  styleUrls: ['./toggles.component.css'],
  templateUrl: './toggles.component.html'
})

export class TogglesComponent {
  constructor() {
  }

  @Input()
  choices: Choice[];

  @Input()
  title2: string;

  @Output() change = new EventEmitter<Choice[]>();

  selectAll() {
    this.choices.forEach(function (ch) {
      ch.setValue(true);
    });
    this.change.emit(this.choices);
  }

  exclusiveSelect(choice) {
    this.choices.forEach(function (ch) {
      if (ch !== choice) ch.setValue(false);
    });
    choice.setValue(true);
    this.change.emit(this.choices);
  }

  select(event, choice) {

    choice.setValue(!choice.getValue());
    this.change.emit(this.choices);
    event.stopPropagation();
  }
}
