import {Component, Input} from "@angular/core";
import {Item, DamageType, Armor, ClassAllowed} from "../services/model/item";
import {Target, Character} from "../services/model/character";
import {Column} from "./column";
import {GearService} from "../services/gear.service";
import {Subject} from "rxjs/Rx";
import {MarkService} from "../services/mark.service";
import {ItemModalComponent} from "./item-modal.component";
import {ParsedFilter} from "../filterform/filter";


@Component({
  selector: 'grid',
  styleUrls: ['./grid.component.css'],
  templateUrl: './grid.component.html'
})

export class GridComponent {

  @Input() public showModalLinks: boolean = true;
  @Input() public allowFavs: boolean = true;

  @Input() public sortColumn: Column;
  @Input() public cols: Column[];

  private filter: ParsedFilter;

  public DamageType = DamageType;

  public showCount: number = 20;

  getShowItems(): Item[]{
    if (this.processedItems==null) return [];
    if (this.showCount===-1) return this.processedItems;
    return this.processedItems.slice(0, this.showCount);
  }


  private noteChanged: Subject<Item> = new Subject<Item>();
  public showFeed: Subject<Item> = new Subject<Item>();



  constructor(private gearService: GearService, private markService: MarkService) {
    this.noteChanged.debounceTime(100).subscribe(
      itm => {
        this.markService.updateItem(itm);
      });
  }

  sortDescending: boolean = true;

  private rawItems: Item[] = [];
  private processedItems: Item[] = [];
  hasCompareItems: boolean = false;

  allHidden(): boolean{
    if (this.rawItems == null) return false;
    if (this.processedItems == null) return false;
    if (this.rawItems.length>0 && this.processedItems.length==0) return true;
    return false;
  }

  count(): number{
    if (this.processedItems==null) return 0;
    return this.processedItems.length;
  }

  totalCount(): number{
    if (this.rawItems==null) return 0;
    return this.rawItems.length;
  }

  show(item: Item){
      this.showFeed.next(item);
  }

  mark(marking: string, item: Item) {
    if (marking === item.mark) marking = null;
    item.mark = marking;
    this.markService.updateItem(item);
  }

  fav(item: Item) {
    if (this.allowFavs!=true) return;
    if (item.fav === null || item.fav === false){
      item.fav = true;
    }
    else{
      item.fav = null;
    }
    this.markService.updateItem(item, true);
  }

  toggleCompare(item: Item){
    if (item.compare===true) item.compare = false;
    else item.compare = true;

    this.processList(this.rawItems, this.filter);
  }

  itemNotesChanged(item: Item) {
    this.noteChanged.next(item);
  }

  private includeItem(itm: Item, filterText: string): boolean{
    let returnMe: boolean = false;
    //check types
    if (this.filter.type.indexOf(itm.type)<0){

    }
    //check tags
    else if (this.filter.tags.indexOf(itm.mark)<0){

    }
    //check damage types
    else if (this.filter.dmgType!=null && this.filter.dmgType!=itm.dmgType){

    }
    //leveled
    else if (this.filter.leveled!=null && this.filter.leveled!=itm.steps.isComplete()){

    }
    //equipped
    else if (this.filter.equipped!=null && this.filter.equipped!=itm.equipped){

    }
    //rarity
    else if (this.filter.rarity.indexOf(itm.tierTypeName)<0){

    }
    //owner
    else if (this.filter.owner.indexOf(itm.owner.label)<0){

    }
    //min light
    else if (this.filter.minLight!=null && this.filter.minLight>itm.light){

    }
    //max light
    else if (this.filter.maxLight!=null && this.filter.maxLight<itm.light){

    }
    //min qual
    else if (this.filter.minQual!=null && this.filter.minQual>(itm as Armor).qualityPct){

    }
    //max qual
    else if (this.filter.maxQual!=null && this.filter.maxQual<(itm as Armor).qualityPct){

    }

    else if (this.filter.statSplit!=null && this.filter.statSplit!=(itm as Armor).statSplit){

    }
    else if (this.filter.classAllowed!=null && this.filter.classAllowed.indexOf(""+itm.classAllowed)<0){

    }
    else if (filterText!=null && ((itm.name.toLowerCase().indexOf(filterText)<0) &&
      (itm.notes!=null && itm.notes.toLowerCase().indexOf(filterText)<0) &&
      (itm.steps!=null && itm.steps.searchText.indexOf(filterText)<0) &&
      (itm.vendor===null || (itm.vendor!=null && itm.vendor.toLowerCase().indexOf(filterText)<0))
      )){

    }
    // else if (filterText!=null && itm.vendor!=null && itm.vendor.indexOf(filterText)<0){
    //
    // }
    else if (this.filter.onlyShowCompare && !itm.compare){

    }
    else{
      returnMe = true;
    }
    // if (!returnMe){
    //   console.log(itm.name);
    // }
    return returnMe;
  }

  private filterList(): Item[]{
    let compCount: number = 0;
    if (this.filter==null){
      return this.rawItems.slice(0);
    }
    let filtered: Item[] = [];
    let filterText: string = null;
    if (this.filter.searchText!=null && this.filter.searchText.length>0){
      filterText = this.filter.searchText.toLowerCase();
    }
    for (let cntr:number=0; cntr<this.rawItems.length; cntr++){
      let itm: Item = this.rawItems[cntr];
      if (this.includeItem(itm, filterText)) {
        filtered.push(itm);
        if (itm.compare===true) compCount++;
      }
    }
    this.hasCompareItems = compCount>=2;
    return filtered;
  }

  public processList(rawItems: Item[], filter: ParsedFilter) {
    this.rawItems = rawItems;
    this.filter = filter;
    let filtered: Item[] = this.filterList();
    if (this.sortColumn != null) {
      let key: string = this.sortColumn.key;
      let desc: boolean = this.sortDescending;
      filtered.sort(
        function (a: Item, b: Item) {
          if (a[key] < b[key]) {
            return desc ? 1 : -1;
          } else if (a[key] > b[key]) {
            return desc ? -1 : 1;
          } else {
            return 0;
          }
        }
      );
    }
    this.processedItems = filtered;
  }

  sort(col: Column) {
    if (this.sortColumn == col) {
      this.sortDescending = !this.sortDescending;
    }
    else {
      this.sortColumn = col;
      this.sortDescending = true;
    }
    this.processList(this.rawItems, this.filter);
  }

  canEquip(itm: Item){
    if (itm.classAllowed === ClassAllowed.Any) return true;
    if (ClassAllowed[itm.classAllowed] === (itm.owner as Character).className)
      return true;
    return false;
  }

  equip(itm: Item) {
    this.gearService.equip(itm)
  }

  transfer(itm: Item, target: Target) {
    this.gearService.transfer(itm, target)
  }

  unlock(itm: Item) {
    this.gearService.unlock(itm);
  }

  lock(itm: Item) {
    this.gearService.lock(itm);
  }

  registerModal(modal: ItemModalComponent){
    this.showFeed.subscribe(
      x => {
        modal.shown = x;
        modal.show();
      }
    );
  }

}
