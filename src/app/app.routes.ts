/**
 * Created by davecaslin on 12/26/16.
 */
import { Routes } from '@angular/router';

import { WeaponsComponent } from './weapons/weapons.component';
import { ArmorComponent } from './armor/armor.component';
import { EngramsComponent } from './engrams/engrams.component';
import { FavsComponent } from './favs/favs.component';
import { CharsComponent } from './chars/chars.component';
import {AboutComponent} from "./about/about.component";
import {BountiesComponent} from "./bounties/bounties.component";

export const rootRouterConfig: Routes = [
  { path: '', redirectTo: 'weapons', pathMatch: 'full' },
  { path: 'weapons', component: WeaponsComponent },
  { path: 'armor', component: ArmorComponent },
  { path: 'engrams', component: EngramsComponent },
  { path: 'favs', component: FavsComponent },
  { path: 'chars', component: CharsComponent },
  { path: 'bounties', component: BountiesComponent },
  { path: 'about', component: AboutComponent }
];
